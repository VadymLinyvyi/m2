define([
    'jquery',
    'ko'
], function($,ko){
        'use strict';
        return function (config){
            return{
                qty : ko.observable(1),
                incrementClick: function () {
                    let counter = this.qty();
                    this.qty(++counter);
                },
                decrementClick: function () {
                    let counter = this.qty();
                    if (counter>0){
                        this.qty(--counter);
                    }

                }
            }
        }
    }
);
